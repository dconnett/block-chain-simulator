import argparse
import blockchainSimulation

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Block chain simulation program, '
                                                 'written by David Connett and Shelli Orzach.',
                                     epilog='Example usage: python runSimulation.py 5')
    parser.add_argument('simulation_rounds', type=int,
                        help='number of rounds to run the simulation, e.g. 10')
    args = parser.parse_args()
    blockchainSimulation.start_simulation(args.simulation_rounds)
