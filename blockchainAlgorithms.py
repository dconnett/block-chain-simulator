import pickle
import sys
import random
import math
import hashlib


def elgamal_signature(p, g, a, t):
    """ElGamal Signature. Note {p, g^a, hash function} compose the public key.
       Args:
        p: Sufficiently large prime number
        g: Generator integer
        a: Random number : 0 < a < p-2
        t: Text to be signed

    Returns:
        r: random?
        s: signature
        t: text

    .. _PEP 484:
        https://www.python.org/dev/peps/pep-0484/

    """
    message = int(hashlib.sha256(t.encode('UTF-8')).hexdigest(), 16)

    k = random.randint(0, p - 1)
    while math.gcd(k, p - 1) != 1:
        k = random.randint(0, p - 1)
    r = pow(g, k, p)
    k_inverse = mod_inverse_recursive(k, p - 1)
    s = (k_inverse * (message - (a * r))) % (p - 1)
    return r, s, t


def extended_gcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        g, y, x = extended_gcd(b % a, a)
        return g, x - (b // a) * y, y


def mod_inverse_recursive(a, m):
    g, x, y = extended_gcd(a, m)
    return x % m


def extended_euclidean_algorithm(a, b):
    """
    Returns a three-tuple (gcd, x, y) such that
    a * x + b * y == gcd, where gcd is the greatest
    common divisor of a and b.

    This function implements the extended Euclidean
    algorithm and runs in O(log b) in the worst case.
    """
    s, old_s = 0, 1
    t, old_t = 1, 0
    r, old_r = b, a

    while r != 0:
        quotient = old_r // r
        old_r, r = r, old_r - quotient * r
        old_s, s = s, old_s - quotient * s
        old_t, t = t, old_t - quotient * t

    return old_r, old_s, old_t


def modular_inverse(n, p):
    """
    Returns the multiplicative inverse of
    n modulo p.

    This function returns an integer m such that
    (n * m) % p == 1.
    """
    gcd, x, y = extended_euclidean_algorithm(n, p)
    assert (n * x + p * y) % p == gcd

    if gcd != 1:
        # Either n is 0, or p is not a prime number.
        raise ValueError(
            '{} has no multiplicative inverse '
            'modulo {}'.format(n, p))
    else:
        return x % p


def elgamal_verification(p, g, x, r, s, t):
    """El Gamel Signature
       Args:
        p: sufficiently large prime number
        g: Generator integer
        x: generator raised to the 'a' power
        r: sent with signature
        s: signature
        t: text to be verified

    Returns:
        bool: Document is valid or invalid

    """
    y = pow(g, x, p)
    v = ((x ** r) * (r ** s)) % p
    v2 = pow(x, r, p) + pow(r, s, p)
    message = int(hashlib.sha256(t.encode('UTF-8')).hexdigest(), 16)

    test = pow(g, message, p)
    # test2 = pow(g, big_bytes, p)
    if v == test:
        return True
    else:
        return False


def farey(limit):
    """Fast computation of Farey sequence as a generator"""
    # n, d is the start fraction n/d (0,1) initially
    # N, D is the stop fraction N/D (1,1) initially
    pend = []
    n = 0
    d = N = D = 1
    while True:
        mediant_d = d + D
        if mediant_d <= limit:
            mediant_n = n + N
            pend.append((mediant_n, mediant_d, N, D))
            N = mediant_n
            D = mediant_d
        else:
            yield n, d
            if pend:
                n, d, N, D = pend.pop()
            else:
                break


def test_functions():
    # Test farey
    for i in farey(4):
        print(i)

    # Test signature
    print(elgamal_signature(13, 11, 4, "Test"))


def generate_elgamal_private_primitives(bits):
    prime = generate_large_prime(bits)
    generator = find_primitive_root(prime)
    return prime, generator


def find_primitive_root(p):
    if p == 2:
        return 1
    p1 = 2
    p2 = (p - 1) // p1
    while True:
        g = random.randint(2, p - 1)
        if not (pow(g, (p - 1) // p1, p) == 1):
            if not pow(g, (p - 1) // p2, p) == 1:
                return g


def generate_large_prime(bits):
    prime_candidate = random_odd(bits)
    rounds = 10
    count = 0
    while not miller_rabin_rounds(prime_candidate, rounds):
        count += 1
        prime_candidate = random_odd(bits)
    return prime_candidate


def random_odd(n):
    """Generates a random odd number of max n bits"""
    a = random.getrandbits(n)
    if a % 2 == 0:
        a -= 1
    return a


def miller_rabin_rounds(n, t):
    """Runs miller-rabin primality test t times on cadidate number n"""

    #  First find the values r and s such that 2^s * r = n - 1
    r = (n - 1) // 2
    s = 1
    while r % 2 == 0:
        s += 1
        r //= 2

    #  Run the test t times
    for i in range(t):
        a = random.randint(2, n - 1)
        y = pow(a, r, n)

        if y != 1 and y != n - 1:
            #  check there is no j for which (a^r)^(2^j) = -1 (mod n)
            j = 0
            while j < s - 1 and y != n - 1:
                y = (y * y) % n
                if y == 1:
                    return False
                j += 1
            if y != n - 1:
                return False

    return True


def mine_block(block):
    """
    Returns a nonce which when hashed with the block object results in two leading 0s
    Real blockchains will have variable leading zeroes with respect to the average mining time
    """
    block_hash = ""
    block_nonce = 0
    while block_hash[0:4] != "0000":
        block_hash = str(hashlib.sha256(pickle.dumps(block) + str(block_nonce).encode()).hexdigest())
        block_nonce += 1
        # print("Mining in progress:", block_nonce, block_hash, end="\r")
        sys.stdout.write("\r" + block_hash)
        sys.stdout.flush()
    return block_nonce - 1
