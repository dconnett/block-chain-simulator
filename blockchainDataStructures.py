import time
from blockchainAlgorithms import *


class Transaction:
    def __init__(self, sender_address, receiver_address, signature, amount):
        self.sender_address = sender_address
        self.receiver_address = receiver_address
        self.signature = signature
        self.amount = amount
        self.timestamp = time.time()

    def get_signature_message(self):
        return str(self.sender_address) + ":" + str(self.receiver_address) + ":" + str(self.amount)


class Block:
    def __init__(self, index, previous_hash, transaction):
        self.index = index
        self.previous_block_hash = previous_hash
        self.timestamp = time.time()
        self.transaction_list = [transaction]
        self.block_hash = ""
        self.address_balances = {}
        self.nonce = ""

    def add_transaction(self, public_key, transaction):
        # Verify Signature
        # Add to block
        if UserKey.verify_transaction(public_key, transaction):
            print("    Transaction to",
                  transaction.receiver_address[0:15] + "... validated! Adding transaction to block!")
            self.update_balance_summary(transaction.receiver_address, transaction.amount)
            self.transaction_list.append(transaction)
        else:
            return "Invalid transaction signature"
        return

    def create_new_block(self, receiver_address, nonce):
        # check if block hash is a hash has leading zeros
        self_hash = hashlib.sha256(pickle.dumps(self) + nonce.encode()).hexdigest()
        if self_hash[0:4] == "0000":
            new_block = Block(self.index + 1, self_hash,
                              Transaction("Block Reward", receiver_address, "Block Reward", 50))
            # Carry over balances
            new_block.address_balances = self.address_balances
            return new_block
        else:
            return "Invalid block"

    def update_balance_summary(self, receiver_address, amount):
        if receiver_address in self.address_balances:
            self.address_balances[receiver_address] = self.address_balances[receiver_address] + amount
        else:
            self.address_balances[receiver_address] = amount


class UserKey:
    def __init__(self, prime=199, generator=5):
        self.prime = prime
        self.generator = generator
        self.private_random = random.randint(0, prime - 2)
        self.public_random = pow(self.generator, self.private_random, self.prime)
        self.address = hashlib.sha256(str(self.prime).encode('UTF-8')).hexdigest() \
                       + hashlib.sha256(str(self.generator).encode('UTF-8')).hexdigest() \
                       + hashlib.sha256(str(self.public_random).encode('UTF-8')).hexdigest()

    def get_private_key(self):
        return self.prime, self.generator, self.private_random

    def get_public_key(self):
        return self.prime, self.generator, self.public_random

    def recalculate_public_random(self):
        self.public_random = pow(self.generator, self.private_random, self.prime)

    def sign_message(self, message):
        return elgamal_signature(self.prime, self.generator, self.private_random, message)

    def send_transaction(self, address, amount):
        message = str(self.address) + ":" + str(address) + ":" + str(amount)
        signature = self.sign_message(message)
        transaction = Transaction(self.address, address, signature, amount)
        return transaction

    @staticmethod
    def verify_transaction(public_key, transaction):
        message = transaction.get_signature_message()
        return elgamal_verification(public_key[0], public_key[1], public_key[2],
                                    transaction.signature[0], transaction.signature[1], message)
