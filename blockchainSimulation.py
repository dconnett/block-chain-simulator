from blockchainDataStructures import *
from blockchainAlgorithms import *
from blockchainAlgorithms import mine_block
import hashlib


def generate_user_keys(number_of_users):
    user_keys = []
    for i in range(number_of_users):
        primitives = generate_elgamal_private_primitives(20)
        # print("User", i, "ElGamal Primitives:", primitives)
        user_keys.append(UserKey(primitives[0], primitives[1]))
    return user_keys


def start_simulation(simulation_rounds):
    # Start by creating Bob and Alice manually walk through basic functions as a test
    print("Generating Bob's Keypair...")
    bob_key = UserKey(prime=7019, generator=5)

    bob_private_key = bob_key.get_private_key()
    bob_public_key = bob_key.get_public_key()

    print("Bob's Private Key", bob_key.get_private_key())
    print("Bob's Public Key", bob_key.get_public_key())
    print("Bob's Address", bob_key.address)

    print("Testing Bob's signature and verification algorithm: signing \"test\"")
    signature = elgamal_signature(bob_private_key[0], bob_private_key[1], bob_private_key[2], "test")
    print("Signature:", signature)
    print("Verify Signature: ", elgamal_verification(bob_public_key[0], bob_public_key[1],
                                                     bob_public_key[2], signature[0],
                                                     signature[1], signature[2]))

    print("\nGenerating Alice's Keypair...")
    alice_key = UserKey()

    print("Alice's Private Key", alice_key.get_private_key())
    print("Alice's Public Key", alice_key.get_public_key())
    print("Alice's Address", alice_key.address)

    transaction = bob_key.send_transaction(alice_key.address, 123)
    print()
    print("Testing transaction, Bob sends Alice 123 Oakland Coins: ", transaction.__dict__)
    print("Transaction Signature: ", transaction.signature)

    print("Test transaction verify: ", alice_key.verify_transaction(bob_public_key, transaction))

    # A block chain is just a list of blocks hashed together
    # In real life we use Merckle trees to store blockchains, but for this extra credit we are skipping this
    # The first block has an arbitrary hash.
    print()
    print("Instantiating blockchain and testing mining...")
    blockchain = [Block(0, hashlib.sha256("Genesis Hash".encode(('UTF-8'))).hexdigest(), transaction)]
    print("Genesis block's arbitrary text hash:", blockchain[0].previous_block_hash)
    # Testing the mining function
    nonce = mine_block(blockchain[-1])
    print("Mined nonce:", nonce)

    # Alice gets 50 Oakland coins
    last_block = blockchain[-1]
    blockchain.append(last_block.create_new_block(alice_key.address, str(nonce)))
    print("Alice submitted nonce for block reward")
    print("Old block's hash:", blockchain[-1].previous_block_hash)

    # Testing generation of random ElGamel primitives
    print("Testing arbitrary size ElGamel primative generator, testing 20 bits")
    print(generate_elgamal_private_primitives(20))
    print()
    print("----------------------------------------")
    print("Starting simulation using", simulation_rounds, "rounds!")
    print("----------------------------------------")

    # start generating blockchain transactions with random users being generated
    # users start mining blocks
    # users with coins randomly create transactions to other users
    user_list = [bob_key, alice_key]
    print("\nGenerating 5 new users")
    user_list.extend(generate_user_keys(5))

    # spreading the wealth
    print("Alice makes it rain...")
    for user in user_list:
        if user.address != alice_key.address:
            blockchain[-1].add_transaction(alice_key.get_public_key(), alice_key.send_transaction(user.address, 5))
    print()
    print("Starting random user generation sequence...")
    # a random person is selected to try mining (normally this would be a multithreaded competition)
    # the chosen winner of the reward will then spread it to random people.
    # a random person joins each loop.
    print(simulation_rounds)
    for _ in range(simulation_rounds):
        new_user = generate_user_keys(1)
        print("New user joined with address:", new_user[0].address[0:15] + "...")
        user_list.extend(new_user)
        lucky_user = random.choice(user_list)
        nonce = mine_block(blockchain[-1])
        print("User", lucky_user.address[0:15] + "... lucked out and mined the block with nonce:", str(nonce))
        last_block = blockchain[-1]
        blockchain.append(last_block.create_new_block(lucky_user.address, str(nonce)))
        print(lucky_user.address[0:15] + "... makes it rain ...")
        for user in random.choices(user_list, k=5):
            if user.address != lucky_user.address:
                blockchain[-1].add_transaction(user.get_public_key(), user.send_transaction(user.address, 5))
                print("    5 Oakland coins sent to: ", user.address[0:15])
        block_balances = []
        for key, value in last_block.address_balances.items():
            block_balances.append(key[0:15] + "... : " + str(value))
        print("Last block's balances:", block_balances)
        print()

    print()
    print("Blockchain simulation finished...")
    print("Total blocks:", len(blockchain))
    print("Total users:", len(user_list))
