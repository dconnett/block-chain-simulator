from setuptools import setup

setup(
    name='block-chain-simulator',
    version='1.0',
    packages=[''],
    url='',
    license='MIT',
    author='Dave Connett',
    author_email='',
    description='Blockchain simulation program using ElGamal.'
)
